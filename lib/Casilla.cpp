#include "Casilla.h"

Casilla::Casilla()
{
    // Toda casilla se inicializa como cubierta, sin mina ni minas alrededor.
    state = CUBIERTA;
    m_mina = NULL;
    numMinasAlrededor = 0;
}

bool const Casilla::getDescubierta()
{
    if (state == DESCUBIERTA)
        return true;
    return false;
}

bool const Casilla::getMarcada()
{
    if (state == MARCADA)
        return true;
    return false;
}

Mina* Casilla::getMina()
{
    return m_mina;
}

const int Casilla::getNumMinasAlrededor()
{
    return numMinasAlrededor;
}

bool Casilla::setMina(Mina& mina)
{
    // Bloqueamos set si ya tiene una mina
    if (m_mina)
        return false;

    // Establecemos mina y reportamos exito
    m_mina = &mina;
    return true;
}

void Casilla::setDescubierta()
{
    state = DESCUBIERTA;
}

void Casilla::setMarcada()
{
    state = MARCADA;
}

void Casilla::setNumMinasAlrededor(int nMinas)
{
    numMinasAlrededor = nMinas;
}

void Casilla::muestraDescubierta(ostream &o)
{
    // Segun la practica este metodo no debe tener en cuenta en absoluto las casillas marcadas: "Este m�todo act�a igual que la sobrecarga del operador de salida, con la �nica diferencia de que, si la casilla est� cubierta, muestra si tiene o no tiene mina y de qu� tipo."
    if (getDescubierta()) // Esta descubierta, su valor depende de lo que haya ahi
    {
        if (getMina()) // Casilla descubierta y con mina, representamos con *.
            o << "*";
        else // Casilla descubierta y sin mina, su valor es el numero de minas circundantes.
            o << getNumMinasAlrededor();
    }
    else // Esta cubierta: "Practica: si la casilla esta cubierta, muestra si tiene o no tiene mina y de qu� tipo."
    {
        // Tiene mina
        if (Mina* mina = getMina())
            mina->imprimeSimbolo(o); // Imprimimos mina
        else // No tiene mina, caracter vacio.
            o << " ";
    }
}

ostream& operator << (ostream& o, Casilla &c)
{
    if (c.getMarcada()) // Casilla marcada, representamos con M.
        o << "M";
    else if (c.getDescubierta()) // Esta descubierta, su valor depende de lo que haya ahi
    {
        if (c.getMina()) // Casilla descubierta y con mina, representamos con *.
            o << "*"; 
        else // Casilla descubierta y sin mina, su valor es el numero de minas circundantes.
            o << c.getNumMinasAlrededor();
    }
    else // Esta sin descubrir ni marcar, su valor estara en blanco
        o << " ";
    return o;
};