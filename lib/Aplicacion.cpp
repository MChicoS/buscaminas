#include "Aplicacion.h"
#include <sstream>
using namespace std;
#include <iostream>

Aplicacion::Aplicacion()
{
}

Aplicacion* Aplicacion::pap = NULL;

Aplicacion* Aplicacion::getInstancia()
{
    if (pap == NULL)
        pap = new Aplicacion();
    return pap;
}

void Aplicacion::liberaInstancia()
{
    pap = NULL;
}

void Aplicacion::run()
{
    // Printeamos opciones iniciales
    cout << "Bienvenido al buscaminas!\n";
    OpcionMenu opcion;
    do
    {
        opcion = gestionarMenu();
        switch (opcion)
        {
            case CONFIGURAR: 
                configurar(); 
                break;
            case JUGAR:
                jugar(); 
                break;
            case SALIR: 
                break;
            default:
                cout << "Opcion incorrecta.\n";
                // Limpiamos buffer para el siguiente intento
                cin.clear();
                cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
        }
    } 
    while (opcion != SALIR);
}

OpcionMenu Aplicacion::gestionarMenu()
{
    // Print de opciones
    cout << "1.- Configurar.\n";
    cout << "2.- Jugar.\n";
    cout << "3.- Salir.\n";
    cout << "Introduce una opcion del menu: ";

    // Lectura de input
    int selectedOption;
    cin >> selectedOption;

    // Output
    return (OpcionMenu)(selectedOption - 1);
}

void Aplicacion::configurar()
{
    int numMinasEst, numMinasBon, dimx, dimy;
    bool isValidSetup;
    cout << "Menu de configuracion:\n";
    do
    {
        do
        {
            // Limpiamos buffer
            numMinasEst = NULL;
            cin.clear();
            cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');

            // Intentamos leer valor
            cout << "Introduce el numero de minas estandar: ";
            cin >> numMinasEst;
        }
        while (!numMinasEst || numMinasEst < 1); // No permitimos ni int no parseable ni menor que 1
        do
        {
            // Limpiamos buffer
            numMinasBon = NULL;
            cin.clear();
            cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');

            // Intentamos leer valor
            cout << "Introduce el numero de minas de bonus: ";
            cin >> numMinasBon;
        }
        while (!numMinasBon || numMinasBon < 1); // No permitimos ni int no parseable ni menor que 1
        do
        {
            // Limpiamos buffer
            dimx = NULL;
            cin.clear();
            cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');

            // Intentamos leer valor
            cout << "Introduce nueva DimX del campo de minas: ";
            cin >> dimx;
        } 
        while (!dimx || dimx < 2); // No permitimos ni int no parseable ni menor que 2.
        do
        {
            // Limpiamos buffer
            dimy = NULL;
            cin.clear();
            cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');

            // Intentamos leer valor
            cout << "Introduce nueva DimY del campo de minas: ";
            cin >> dimy;
        } 
        while (!dimy || dimy < 2); // No permitimos ni int no parseable ni menor que 2.

        // Comprobamos si el numero de minas cabe en las dimensiones establecidas
        if (numMinasBon + numMinasEst > dimx * dimy)
        {
            // Reportamos error
            cout << "\nHas introducido mas minas de las que caben en las dimensiones definidas. Intentelo de nuevo:\n";

            // Forzamos repeticion
            isValidSetup = false;
        }
        else // Es valido, permitimos salida del while
            isValidSetup = true;
    } 
    while (!isValidSetup);

    // Establecemos valores
    setNumMinasEst(numMinasEst);
    setNumMinasBonus(numMinasBon);
    setDimX(dimx);
    setDimY(dimy);

    // Reportamos exito
    cout << "La configuracion se ha completado con exito.\n";
}

int Aplicacion::jugar()
{
    // Generamos objetos
    crearCampoMinas();
    crearArrayMinas();

    // Prevencion de nullptr exceptions
    if (!cm)
        return 0;
    if (!arrayMinas)
        return 0;

    // Colocamos minas
    if (!cm->colocaMinas(arrayMinas)) // Si devuelve false, volvemos al menu principal
        return 0;

    // Variable de control de salida del loop infinito de juego
    bool endGame = false;

    // Loop infinito del juego
    do
    {
        // Pedir la introduccion de coordenadas
        cout << "Introduce coordenadas (X-Y) [Para salir (0-0)]: ";
        Coordenada* inputCoordinate = new Coordenada();
        cin >> *inputCoordinate;

        // Escribimos coordenadas para concatenarlas con el string previo a modo de display de las coordenadas elegidas
        cout << *inputCoordinate;

        // Comprobamos si es 0-0
        if (inputCoordinate->getCoordX() == 0 && inputCoordinate->getCoordY() == 0)
            endGame = true;
        else // No es 0-0, ergo es una coordenada valida
        {
            cout << "Desea marcar (M) o descubrir (D) la casilla ";
            cout << *inputCoordinate; // Escribimos las coordenads escritas
            cout << "?: "; // Salto de linea
            OpcionCasilla opcionCasilla;
            do
            {
                char inputChar;
                cin >> inputChar;
                opcionCasilla = (OpcionCasilla)(toupper(inputChar)); // Convertimos a mayuscula para que m y d se acepten tambien.
                switch (opcionCasilla)
                {
                    case MARCAR:
                        // Marcamos la casilla
                        if (!cm->marcaCasilla(*inputCoordinate)) // si devuelve false, la casilla no se puede marcar (por outrange de coordenadas, porque estaba ya marcada o por que estaba descubierta), y volvemos a intentar.
                            cout << "\nNo se puede marcar esta casilla.\n"; // Reportamos error.
                        break; // Abandonamos este loop para forzar la eleccion de otra coordenada
                    case DESCUBRIR:
                        // Descubre casilla
                        if (!cm->descubreCasilla(*inputCoordinate)) // Si devuelve false, la casilla no se puede descubrir(por outrange de coordenadas o por que estaba ya descubierta), y volvemos a intentar.
                            cout << "\nNo se puede descubrir esta casilla.\n"; // Reportamos error.
                        else if (cm->hasMina(*inputCoordinate)) // Si las coordenadas elegidas para descubrir tienen una mina, forzar gameover.
                            endGame = true;
                        break;  // Abandonamos este loop para forzar la eleccion de otra coordenada
                    default:
                        // Caracter diferente de M y D
                        cout << "El caracter introducido no corresponde a Marcar(M) ni a Descubrir(D), intentelo de nuevo: \n";
                        break;
                }
            }  // Permanecera en este loop hasta que la seleccion de marcar/descubrir sea valida.
            while (opcionCasilla != MARCAR && opcionCasilla != DESCUBRIR);

            // Actualizar tablero (display)
            cout << *cm;
        }
    } 
    while (!endGame);

    // Si estamos en este punto es que hemos abandonado el While debido a pisar una mina o introducir 0-0
    // Calculamos puntos.
    int puntos = cm->calculaPuntos();

    // Mostramos los puntos.
    ostringstream endGameMsg;
    endGameMsg << "\nFin del juego, puntuacion obtenida: " << puntos << " puntos.\n\n";
    cout << endGameMsg.str();

    // Borramos variables del juego
    liberarCampoMinas();
    liberarArrayMinas();

    return 1;
}

void Aplicacion::crearCampoMinas()
{
    Aplicacion::cm = new CampoMinas(dimx, dimy);
}

void Aplicacion::crearArrayMinas()
{
    // Generamos array temporal
    Mina** arrayMinasTemp = new Mina*[getNumMinas()];

    // Lo cargamos de minas de los tipos adecuados 
    for (int i = 0; i < getNumMinas(); i++)
    {
        // Rellenamos en primer lugar las minas estandar
        if (i < getNumMinasEst())
            arrayMinasTemp[i] = new MinaEstandar();
        else // El espacio sobrante lo rellenamos de minas bonus
            arrayMinasTemp[i] = new MinaBonus();
    }

    // Guardamos el array temporal generado en la variable de clase Aplicacion
    arrayMinas = arrayMinasTemp;
}

void Aplicacion::setNumMinasEst(int value)
{
    numMinasEst = value;
}

void Aplicacion::setNumMinasBonus(int value)
{
    numMinasBonus = value;
}

void Aplicacion::setDimX(int value)
{
    dimx = value;
}

void Aplicacion::setDimY(int value)
{
    dimy = value;
}

const int Aplicacion::getNumMinas()
{
    return getNumMinasBonus() + getNumMinasEst();
}

const Mina** Aplicacion::getArrayMinas()
{
    return const_cast<const Mina**>(arrayMinas);
}

const int Aplicacion::getNumMinasBonus()
{
    return numMinasBonus;
}

const int Aplicacion::getNumMinasEst()
{
    return numMinasEst;
}

const int Aplicacion::getDimX()
{
    return dimx;
}

const int Aplicacion::getDimY()
{
    return dimy;
}

const CampoMinas* Aplicacion::getCampoMinas()
{
    return cm;
}

void Aplicacion::setArrayMinas(Mina** am)
{
    arrayMinas = am;
}

void Aplicacion::setCampoMinas(CampoMinas* campoM)
{
    cm = campoM;
}

void Aplicacion::liberarCampoMinas()
{
    cm = NULL;
}

void Aplicacion::liberarArrayMinas()
{
    arrayMinas = NULL;
}