#include <iostream>
#include <sstream>
using namespace std;

#include "Coordenada.h"
#include "Aplicacion.h"

Coordenada::Coordenada(int x, int y) : coordX(0), coordY(0)
{
    // Establecemos coordenadas 0 en el instance
    setCoordX(x);
    setCoordY(y);
}

// Funcion de soporte para la generacion de numeros aleatorios en un intervalo.
int getRandomInRange(int min, int max)
{
    return min + (int)((double)rand() / (RAND_MAX + 1) * (max - min + 1));
}

Coordenada* Coordenada::getCoordenadaAleatoria(int limiteSupX, int limiteSupY)
{
    // Generamos coordenadas aleatorias con funcion de soporte y las fijamos
    int ranX = getRandomInRange(1, limiteSupX);
    int ranY = getRandomInRange(1, limiteSupY);
    return new Coordenada(ranX, ranY);
}

int Coordenada::getCoordX() const
{
    return coordX;
}

int Coordenada::getCoordY() const
{
    return coordY;
}

bool Coordenada::setCoordX(int value)
{
    // Bloqueamos valores inferiores a 1 (imposicion de la memoria)
    if (value < 1)
        return false;

    // Establecemos el valor y reportamos exito
    coordX = value;
    return true;
}

bool Coordenada::setCoordY(int value)
{
    // Bloqueamos valores inferiores a 1 (imposicion de la memoria)
    if (value < 1)
        return false;

    // Establecemos el valor y reportamos exito
    coordY = value;
    return true;
}

ostream& operator << (ostream& o, Coordenada& c)
{
    o << "(" << c.coordX << "-" << c.coordY << ")\n";
    return o;
}

istream& operator >> (istream& i, Coordenada& c)
{
    // Forzamos a CIN a ignorar los whitespaces y saltos de linea previos.
    i.ignore(std::numeric_limits<std::streamsize>::max(), '\n');

    // Leemos input
    string inputString;
    i >> inputString;

    // Logica de comprobacion de formato
    bool isValidCoordinate = true; // Variable que controla que se cumplan todas las restricciones de formato
    string leftString;
    string rightString;
    int finalCoordX = 0;
    int finalCoordY = 0;

    // Buscamos el "-" que divide ambas coordenadas, si no lo encontramos o si encontramos mas de uno: Formato erroneo.
    if (count(inputString.begin(), inputString.end(), '-') != 1)
        isValidCoordinate = false;
    else
    {
        // Encontrado "-", separamos los substrings laterales
        leftString = inputString.substr(0, inputString.length() - inputString.find("-") - 1);
        rightString = inputString.substr(inputString.find("-") + 1, inputString.length() - inputString.find("-") - 1);
        // Si alguno de los dos substrings vacio: Formato erroneo.
        if (leftString == "" || rightString == "")
            isValidCoordinate = false;
        else
        {
            // Si el primer caracter del string izquierdo no es un parentesis abierto: Formato erroneo.
            // Si el ultimo caracter del string derecho no es un parentesis cerrado: Formato erroneo.
            if (leftString.find("(") != 0 || rightString.find(")") != (rightString.length() - 1))
                isValidCoordinate = false;
            else
            {
                // Eliminamos los parentesis ahora que ya no nos hacen faltapara parsear el integer correctamente
                leftString = leftString.substr(1, leftString.length() - 1);
                rightString = rightString.substr(0, rightString.length() - 1);

                // Si encontramos algun caracter no numerico en alguno de los dos substrings: Formato erroneo.
                stringstream leftStringStream(leftString);
                if ((leftStringStream >> finalCoordX).fail()) // Si esto tiene exito ya tendremos la variable parseada
                    isValidCoordinate = false;
                else
                {
                    // Si la coordenada Y excede al size Y del tablero: Formato erroneo.
                    if (finalCoordX > Aplicacion::getInstancia()->getDimX())
                        isValidCoordinate = false;
                }
                stringstream rightStringStream(rightString);
                if ((rightStringStream >> finalCoordY).fail()) // Si esto tiene exito ya tendremos la variable parseada
                    isValidCoordinate = false;
                else
                {
                    // Si la coordenada X excede al size X del tablero: Formato erroneo.
                    if (finalCoordY > Aplicacion::getInstancia()->getDimY())
                        isValidCoordinate = false;
                }
            }
        }
    }

    // Verificamos que la coordenada obtenida cumpla todas las especificaciones
    if (isValidCoordinate)
    {
        // Escribimos valores
        c.setCoordX(finalCoordX);
        c.setCoordY(finalCoordY);

        // Reportamos exito si no es la coordenada 0-0 (en ese caso es el fin del juego y el mensaje correspondiente se llama desde Aplicacion)
        if (finalCoordX != 0 || finalCoordY != 0)
            cout << "Coordenada correctas.\n";
    }
    else // No es valida
    {
        // Asignamos valores base
        c.setCoordX(0);
        c.setCoordY(0);

        // Reportamos error
        cout << "\nLas Coordenadas introducidas no son validas.\n";
    }

    return i;
}

bool Coordenada::operator != (const Coordenada& c)
{
    // En la practica no especifica lo que debe hacer esta sobrecarga asi que supongo que ser� esto...
    if (getCoordX() != c.getCoordX())
        return false;
    if (getCoordY() != c.getCoordY())
        return false;
    return true;
}

const Coordenada& Coordenada::operator = (const Coordenada& c) const
{
    // En la practica no especifica lo que debe hacer esta sobrecarga asi que supongo que ser� esto...
    const Coordenada* coord = this;
    coord = &c;
    return *coord;
}