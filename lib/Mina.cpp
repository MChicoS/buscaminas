#include "Mina.h"

int MinaEstandar::puntos = 1;
int MinaBonus::puntos = 5;

Mina::Mina()
{
    simbolo = 'U'; // Unknown tipo de mina
}

MinaEstandar::MinaEstandar()
{
    // Mina estandar: Caracter 'E' y 1 punto.
    simbolo = 'E';
    puntos = 1;
}

MinaBonus::MinaBonus()
{
    // Mina estandar: Caracter 'B' y 5 puntos.
    simbolo = 'B';
    puntos = 5;
}

const int MinaBonus::getPuntos()
{
    return puntos;
}

const int MinaEstandar::getPuntos()
{
    return puntos;
}

const void Mina::imprimeSimbolo(ostream& o)
{
    o << simbolo;
}