#include "CampoMinas.h"
#include "Aplicacion.h"
#include "Coordenada.h"

CampoMinas::CampoMinas(int dX, int dY) : dimx(dX), dimy(dY)
{
    // Establecemos longitud del vector principal (X)
    casilla.resize(dX);

    // Rellenamos lista de casillas
    for (int i = 0; i < dX; i++)
        for (int j = 0; j < dY; j++)
            casilla[i].push_back(new Casilla()); // Llenamos de casillas
}

Casilla& CampoMinas::getCasilla(const Coordenada& c)
{
    return *casilla[c.getCoordX() - 1].at(c.getCoordY() - 1);
}

int const CampoMinas::getDimX()
{
    return dimx;
}

int const CampoMinas::getDimY()
{
    return dimy;
}

void CampoMinas::setNumMinasAlrededorCasilla(const Coordenada& coord)
{
    // Numero de minas temporal
    int numeroMinas = 0;

    // Comprobamos para todas las casillas circundantes a la parametrica
    // |c02|c12|c22|
    // |c01|c11|c12|
    // |c00|c10|c20|
    Coordenada* c00 = new Coordenada(coord.getCoordX() - 1, coord.getCoordY() - 1);
    Coordenada* c10 = new Coordenada(coord.getCoordX(), coord.getCoordY() - 1);
    Coordenada* c20 = new Coordenada(coord.getCoordX() + 1, coord.getCoordY() - 1);
    Coordenada* c01 = new Coordenada(coord.getCoordX() - 1, coord.getCoordY());
    // c11 seria la propia casilla
    Coordenada* c21 = new Coordenada(coord.getCoordX() + 1, coord.getCoordY());
    Coordenada* c02 = new Coordenada(coord.getCoordX() - 1, coord.getCoordY() + 1);
    Coordenada* c12 = new Coordenada(coord.getCoordX(), coord.getCoordY() + 1);
    Coordenada* c22 = new Coordenada(coord.getCoordX() + 1, coord.getCoordY() + 1);

    // Comprobamos para todas ellas e incrementamos si se encuentra.
    // El metodo hasMina cubre en su propio cuerpo la excepcion del caso en el que la casilla objetivo este en una esquina y no tenga alguna de las circundantes definidas
    if (hasMina(*c00))
        numeroMinas++;
    if (hasMina(*c10))
        numeroMinas++;
    if (hasMina(*c20))
        numeroMinas++;
    if (hasMina(*c01))
        numeroMinas++;
    if (hasMina(*c21))
        numeroMinas++;
    if (hasMina(*c02))
        numeroMinas++;
    if (hasMina(*c12))
        numeroMinas++;
    if (hasMina(*c22))
        numeroMinas++;

    // Pequeno hack: Prevencion de out of range exception en el metodo getCasilla
    if (coord.getCoordX() <= 0 || coord.getCoordY() <= 0 || coord.getCoordX() > dimx || coord.getCoordY() > dimy)
        return;

    // Almacenamos valor
    getCasilla(coord).setNumMinasAlrededor(numeroMinas);
}

const bool CampoMinas::hasMina(const Coordenada& c)
{
    // Pequeno hack: Prevencion de out of range exception en el metodo getCasilla
    if (c.getCoordX() <= 0 || c.getCoordY() <= 0 || c.getCoordX() > dimx || c.getCoordY() > dimy)
        return false;

    // Obtenemos mina de la casilla deseada
    if (getCasilla(c).getMina())
        return true; // Si no es null, es que tiene mina

    // Llegados a este punto, reportar fracaso.
    return false;
}

bool CampoMinas::colocaMinas(Mina** mina)
{
    // Leemos numero total de minas de la clase Aplicacion estatica
    int numMinas = Aplicacion::getInstancia()->getNumMinas();

    // Maximos intentos posibles: Variable incremental
    int intentos = 0;
    // Colocamos el numero de minas adecuado
    for (int i = 0; i < numMinas;) // Incrementaremos el counter solo cuando insertemos una mina con exito
    {
        // Violacion de intentos maximos
        if (intentos >= MAX_INTENTOS)
            return false;

        // Obtenemos una coordenada aleatoria donde colocar la mina
        Coordenada* ranCord = Coordenada::getCoordenadaAleatoria(dimx, dimy);

        // Pequeno hack: Prevencion de out of range exception en el metodo getCasilla
        if (ranCord->getCoordX() <= 0 || ranCord->getCoordY() <= 0 || ranCord->getCoordX() > dimx || ranCord->getCoordY() > dimy)
            continue;

        // Si las coordenadas elegidas ya tienen una mina, ignorar vuelta actual y repetir para obtener otras coordenadas.
        if (hasMina(*ranCord)) // Incrementamos contador de intentos
            intentos++;
        else // Coordenadas libres,asignamos mina.
        {
            // Insertamos mina
            getCasilla(*ranCord).setMina(*mina[i]);

            // Una mina colocada correctamente debe reiniciar el contador
            intentos = 0;

            // Incrementamos minas colocadas
            i++;
        }
    }

    // Establecimiento de minas alrededor de casillas
    // Barremos todas las casillas que hay
    for (int i = 1; i <= dimx; i++)
        for (int j = 1; j <= dimy; j++)
            setNumMinasAlrededorCasilla(*new Coordenada(i, j)); // Establecemos para la coordenada correspondiente

    // Reportamos exito
    return true;
}

int CampoMinas::calculaPuntos()
{
    // Variable output
    int puntosTotales = 0;

    // Iteramos todas las casillas
    for (int i = 1; i <= dimx; i++)
        for (int j = 1; j <= dimy; j++)
            if (Casilla* cas = &getCasilla(*new Coordenada(i, j))) // Leemos la casilla de esta posicion
            {
                // La casilla no esta marcada: Abortar iteracion.
                if (!cas->getMarcada())
                    continue;

                // Leemos los puntos de la mina que hay en esta casilla
                if (Mina* min = cas->getMina())
                    puntosTotales += min->getPuntos();
                else // Si no hay mina, penalizamos al usuario con la perdida de un punto
                    puntosTotales--;
            }

    // Output
    return puntosTotales;
}

bool CampoMinas::descubreCasilla(const Coordenada &coord)
{
    // Pequeno hack: Prevencion de out of range exception en el metodo getCasilla
    if (coord.getCoordX() <= 0 || coord.getCoordY() <= 0 || coord.getCoordX() > dimx || coord.getCoordY() > dimy)
        return false;

    // Se puede perfectamente descubrir una casilla marcada por lo que no necesitamos cubrir ese caso

    // Comprobamos que no estaba ya descubierta
    if (getCasilla(coord).getDescubierta())
        return false;

    // Descubrimos la casilla y reportamos exito
    getCasilla(coord).setDescubierta();
    return true;
}

bool CampoMinas::marcaCasilla(const Coordenada& coord)
{
    // Pequeno hack: Prevencion de out of range exception en el metodo getCasilla
    if (coord.getCoordX() <= 0 || coord.getCoordY() <= 0 || coord.getCoordX() > dimx || coord.getCoordY() > dimy)
        return false;

    // Comprobamos que no estaba ya descubierta
    if (getCasilla(coord).getDescubierta())
        return false;

    // Comprobamos que no estaba ya marcada
    if (getCasilla(coord).getMarcada())
        return false;

    // Descubrimos la casilla y reportamos exito
    getCasilla(coord).setMarcada();
    return true;
}


ostream& operator << (ostream&o, const CampoMinas &cm)
{
    // Generamos filas
    for (int j = cm.dimy; j > 0; j--)
    {
        // Numero de fila y separador inicial
        o << "\n" << j << "|";

        // Valor de cada columna para la fila actual
        for (int i = 1; i <= cm.dimx; i++)
        {
            // Leemos casilla
            Casilla* c = cm.casilla[i - 1].at(j - 1);
            
            // Printeamos su simbolo (desde el operador sobrecargado de Casilla)
            o << *c;

            // ... o desde el metodo muestra descubierta (mostrar valor de la mina de las casillas no descubiertas) (Bloqueado porque no nos piden su utilizacion directa, es mas para funciones de debug)
            //c->muestraDescubierta(o);

            // Barra final es constante
            o << "|";
        }
    }

    // Fila final con identificadores de columna
    o << "\n  "; // Saltamos de linea y 4 espacios para que el numero coincida con la primera columna
    for (int i = 1; i <= cm.dimx; i++)
        o << i << " ";

    // Salto de linea tras el tablero
    o << "\n\n";

    return o;
}