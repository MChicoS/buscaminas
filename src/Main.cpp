#include "Aplicacion.h"

int main()
{
    // Creamos instance de la aplicacion y la ejecutamos
    Aplicacion* a = Aplicacion::getInstancia();
    a->run();

    // Destruimos instancia
    Aplicacion::liberaInstancia();

    return(1);
}