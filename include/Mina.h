#ifndef MINA_H
#define MINA_H

using namespace std;
#include <iostream>

class Mina
{
    protected:
        char simbolo;
    public:
        Mina();
        ~Mina();
        virtual const int getPuntos() = 0;
        const void imprimeSimbolo(ostream& o);
};

class MinaEstandar : public Mina
{
    private:
        static int puntos;
    public:
        MinaEstandar();
        ~MinaEstandar();
        const int getPuntos();
};

class MinaBonus : public Mina
{
    private:
        static int puntos;
    public:
        MinaBonus();
        ~MinaBonus();
        const int getPuntos();
};

#endif
