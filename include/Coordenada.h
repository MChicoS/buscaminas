#ifndef COORDENADA_H
#define COORDENADA_H

class Coordenada
{
    private:
        int coordX;
        int coordY;
    public:
        Coordenada(int x = 0, int y = 0);
        Coordenada coordenada(Coordenada& c);
        ~Coordenada();

        // Metodos de lectura de coordenadas
        int getCoordX() const;
        int getCoordY() const;
        bool setCoordX(int value);
        bool setCoordY(int value);

        // Extra
        static Coordenada* getCoordenadaAleatoria(int limiteSupX, int limiteSupY);

        // Operators
        friend ostream& operator << (ostream& o, Coordenada& c);
        friend istream& operator >> (istream& i, Coordenada& c);
        const Coordenada& operator = (const Coordenada& c) const;
        bool operator != (const Coordenada& c);
};
#endif
