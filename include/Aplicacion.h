#ifndef APLICACION_H
#define APLICACION_H

#include "Mina.h"
#include "CampoMinas.h"

enum OpcionMenu
{
    CONFIGURAR = 0,
    JUGAR = 1,
    SALIR = 2,
};

enum OpcionCasilla
{
    MARCAR = 'M',
    DESCUBRIR = 'D',
};

class Aplicacion
{
    protected:
        Aplicacion();
        void crearCampoMinas();
        void crearArrayMinas();
        void liberarCampoMinas();
        void liberarArrayMinas();
    private:
        static Aplicacion* pap;
        Mina** arrayMinas = NULL;
        CampoMinas* cm = NULL;
        int dimx = 9;
        int dimy = 9;
        int numMinasEst = 8;
        int numMinasBonus = 2;
    public:
        static Aplicacion* getInstancia();
        static void liberaInstancia();
        ~Aplicacion();
        const int getNumMinas();
        const int getNumMinasBonus();
        const int getNumMinasEst();
        const int getDimX();
        const int getDimY();
        const Mina** getArrayMinas();
        const CampoMinas* getCampoMinas();

        void setNumMinasBonus(int nm);
        void setNumMinasEst(int nm);
        void setDimX(int dx);
        void setDimY(int dy);

        void setArrayMinas(Mina** am);
        void setCampoMinas(CampoMinas* campoM);
        
        void run();
        OpcionMenu gestionarMenu();
        void configurar();
        int jugar();
};
#endif // APLICACION_H
