#ifndef CAMPOMINAS_H
#define CAMPOMINAS_H

using namespace std;
#include <vector>
#include "Coordenada.h"
#include "Casilla.h"
#include "Mina.h"

const int MAX_INTENTOS = 10;

class CampoMinas
{
    private:
        const int dimx;
        const int dimy;
        vector<vector<Casilla*>> casilla;
    public:
        CampoMinas(int dx = 9, int dy = 9);
        CampoMinas(CampoMinas& c);
        ~CampoMinas();

        int const getDimX();
        int const getDimY();
        Casilla& getCasilla(const Coordenada& c);
        const bool hasMina(const Coordenada& c);
        bool marcaCasilla(const Coordenada& c);
        bool descubreCasilla(const Coordenada& c);
        bool colocaMinas(Mina** m);
        int calculaPuntos();
        void setNumMinasAlrededorCasilla(const Coordenada& c);

        // Operators
        friend ostream& operator << (ostream&o, const CampoMinas &cm);
};
#endif // CAMPOMINAS_H
