#ifndef CASILLA_H
#define CASILLA_H

#include "Mina.h"

enum EstadoCasilla
{
    CUBIERTA = 0,
    MARCADA = 1,
    DESCUBIERTA = 2,
};

class Casilla
{
    private:
        EstadoCasilla state;
        int numMinasAlrededor;
        Mina* m_mina;
    public:
        Casilla();
        Casilla(Casilla& c);
        ~Casilla();
        const bool getDescubierta();
        void setDescubierta();
        const bool getMarcada();
        void setMarcada();
        Mina* getMina();
        bool setMina(Mina& mina);
        const int getNumMinasAlrededor();
        void setNumMinasAlrededor(int nMinas);
        void muestraDescubierta(ostream &o);
        
        // Operators
        friend  ostream& operator << (ostream& o, Casilla &c);
};
#endif
